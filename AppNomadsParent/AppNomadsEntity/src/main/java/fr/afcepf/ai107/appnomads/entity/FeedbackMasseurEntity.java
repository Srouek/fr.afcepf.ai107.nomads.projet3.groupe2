package fr.afcepf.ai107.appnomads.entity;

import java.util.Date;

public class FeedbackMasseurEntity
{
	int idFeedbackMasseur;
	String commentaire;
	float chiffreDAffaire;
	int nbMassage;
	Date dateFeedback;
	int noteAccueil;
	int noteAmbiance;
	int noteClientele;
	
	PrestationEntity prestation;
	// List de Masseurs
	
	
	

	public int getIdFeedbackMasseur() {
		return idFeedbackMasseur;
	}

	public void setIdFeedbackMasseur(int idFeedbackMasseur) {
		this.idFeedbackMasseur = idFeedbackMasseur;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public float getChiffreDAffaire() {
		return chiffreDAffaire;
	}

	public void setChiffreDAffaire(float chiffreDAffaire) {
		this.chiffreDAffaire = chiffreDAffaire;
	}

	public int getNbMassage() {
		return nbMassage;
	}

	public void setNbMassage(int nbMassage) {
		this.nbMassage = nbMassage;
	}

	public Date getDateFeedback() {
		return dateFeedback;
	}

	public void setDateFeedback(Date dateFeedback) {
		this.dateFeedback = dateFeedback;
	}

	public int getNoteAccueil() {
		return noteAccueil;
	}

	public void setNoteAccueil(int noteAccueil) {
		this.noteAccueil = noteAccueil;
	}

	public int getNoteAmbiance() {
		return noteAmbiance;
	}

	public void setNoteAmbiance(int noteAmbiance) {
		this.noteAmbiance = noteAmbiance;
	}

	public int getNoteClientele() {
		return noteClientele;
	}

	public void setNoteClientele(int noteClientele) {
		this.noteClientele = noteClientele;
	}

	public PrestationEntity getPrestation() {
		return prestation;
	}

	public void setPrestation(PrestationEntity prestation) {
		this.prestation = prestation;
	}
	
}
