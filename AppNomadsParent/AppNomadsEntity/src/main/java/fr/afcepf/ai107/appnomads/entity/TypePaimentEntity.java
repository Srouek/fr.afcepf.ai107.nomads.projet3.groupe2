package fr.afcepf.ai107.appnomads.entity;

import java.util.List;

public class TypePaimentEntity
{	
	int typePaiment;
	String Libelle;
	
	List<PrestationEntity> prestations;

	/**
	 * @param typePaiment
	 * @param libelle
	 * @param prestations
	 */
	public TypePaimentEntity(int typePaiment, String libelle, List<PrestationEntity> prestations)
	{
		super();
		this.typePaiment = typePaiment;
		Libelle = libelle;
		this.prestations = prestations;
	}

	public int getTypePaiment() {
		return typePaiment;
	}

	public void setTypePaiment(int typePaiment) {
		this.typePaiment = typePaiment;
	}

	public String getLibelle() {
		return Libelle;
	}

	public void setLibelle(String libelle) {
		Libelle = libelle;
	}

	public List<PrestationEntity> getPrestations() {
		return prestations;
	}

	public void setPrestations(List<PrestationEntity> prestations) {
		this.prestations = prestations;
	}
	
	
}
