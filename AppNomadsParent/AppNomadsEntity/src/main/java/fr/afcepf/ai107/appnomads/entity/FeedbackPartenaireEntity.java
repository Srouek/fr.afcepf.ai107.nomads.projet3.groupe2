package fr.afcepf.ai107.appnomads.entity;

import java.util.Date;

public class FeedbackPartenaireEntity
{
	int idFeedbackPartenaire;
	String commentaire;
	Date dateFeedback;
	int noteRelationnel;
	int ponctualite;
	int noteQualiteMassage;
	
	PrestationEntity prestation;
	// List de Partenaires
	
	

	public int getIdFeedbackPartenaire() {
		return idFeedbackPartenaire;
	}

	public void setIdFeedbackPartenaire(int idFeedbackPartenaire) {
		this.idFeedbackPartenaire = idFeedbackPartenaire;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Date getDateFeedback() {
		return dateFeedback;
	}

	public void setDateFeedback(Date dateFeedback) {
		this.dateFeedback = dateFeedback;
	}

	public int getNoteRelationnel() {
		return noteRelationnel;
	}

	public void setNoteRelationnel(int noteRelationnel) {
		this.noteRelationnel = noteRelationnel;
	}

	public int getPonctualite() {
		return ponctualite;
	}

	public void setPonctualite(int ponctualite) {
		this.ponctualite = ponctualite;
	}

	public int getNoteQualiteMassage() {
		return noteQualiteMassage;
	}

	public void setNoteQualiteMassage(int noteQualiteMassage) {
		this.noteQualiteMassage = noteQualiteMassage;
	}

	public PrestationEntity getPrestation() {
		return prestation;
	}

	public void setPrestation(PrestationEntity prestation) {
		this.prestation = prestation;
	}

}
