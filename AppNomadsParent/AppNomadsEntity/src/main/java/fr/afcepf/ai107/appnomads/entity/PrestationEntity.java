package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PrestationEntity implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	int idPrestation;
	String libelle;
	Date datePrestation;
	Date heureDebut;
	Date heureFin;
	Date dureeDemandeeParMassage;
	int nbMasseur;
	int nbChaiseErgonomiques;
	float coutsInscription;
	String numeroFacture;
	Date dateFacture;
	float montantFacture;
	float montantRemuneration;
	Date paimentFacture;
	Date annulation;
	String spécificite;
	int idPrestationMere;
	
	ReccurenceEntity reccurence;
	TypePaimentEntity typePaiment;
	MotifAnnulationEntity motifAnnulation;
	List<FeedbackPartenaireEntity> retoursPartenaire;
	List<FeedbackMasseurEntity> retoursMasseur;
	AdresseEntity adresse;
	// Emprunt Materiel
	
	
	
	
	
	
	
	/**
	 * @param idPrestation
	 * @param libelle
	 * @param datePrestation
	 * @param heureDebut
	 * @param heureFin
	 * @param dureeDemandeeParMassage
	 * @param nbMasseur
	 * @param nbChaiseErgonomiques
	 * @param coutsInscription
	 * @param numeroFacture
	 * @param dateFacture
	 * @param montantFacture
	 * @param montantRemuneration
	 * @param paimentFacture
	 * @param annulation
	 * @param spécificite
	 * @param idPrestationMere
	 * @param reccurence
	 * @param typePaiment
	 * @param motifAnnulation
	 */
	public PrestationEntity(int idPrestation, String libelle, Date datePrestation, Date heureDebut, Date heureFin,
			Date dureeDemandeeParMassage, int nbMasseur, int nbChaiseErgonomiques, float coutsInscription,
			String numeroFacture, Date dateFacture, float montantFacture, float montantRemuneration,
			Date paimentFacture, Date annulation, String spécificite, int idPrestationMere, ReccurenceEntity reccurence,
			TypePaimentEntity typePaiment, MotifAnnulationEntity motifAnnulation)
	{
		super();
		this.idPrestation = idPrestation;
		this.libelle = libelle;
		this.datePrestation = datePrestation;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.dureeDemandeeParMassage = dureeDemandeeParMassage;
		this.nbMasseur = nbMasseur;
		this.nbChaiseErgonomiques = nbChaiseErgonomiques;
		this.coutsInscription = coutsInscription;
		this.numeroFacture = numeroFacture;
		this.dateFacture = dateFacture;
		this.montantFacture = montantFacture;
		this.montantRemuneration = montantRemuneration;
		this.paimentFacture = paimentFacture;
		this.annulation = annulation;
		this.spécificite = spécificite;
		this.idPrestationMere = idPrestationMere;
		this.reccurence = reccurence;
		this.typePaiment = typePaiment;
		this.motifAnnulation = motifAnnulation;
	}
	
	
	public int getIdPrestation() {
		return idPrestation;
	}
	public void setIdPrestation(int idPrestation) {
		this.idPrestation = idPrestation;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Date getDatePrestation() {
		return datePrestation;
	}
	public void setDatePrestation(Date datePrestation) {
		this.datePrestation = datePrestation;
	}
	public Date getHeureDebut() {
		return heureDebut;
	}
	public void setHeureDebut(Date heureDebut) {
		this.heureDebut = heureDebut;
	}
	public Date getHeureFin() {
		return heureFin;
	}
	public void setHeureFin(Date heureFin) {
		this.heureFin = heureFin;
	}
	public Date getDureeDemandeeParMassage() {
		return dureeDemandeeParMassage;
	}
	public void setDureeDemandeeParMassage(Date dureeDemandeeParMassage) {
		this.dureeDemandeeParMassage = dureeDemandeeParMassage;
	}
	public int getNbMasseur() {
		return nbMasseur;
	}
	public void setNbMasseur(int nbMasseur) {
		this.nbMasseur = nbMasseur;
	}
	public int getNbChaiseErgonomiques() {
		return nbChaiseErgonomiques;
	}
	public void setNbChaiseErgonomiques(int nbChaiseErgonomiques) {
		this.nbChaiseErgonomiques = nbChaiseErgonomiques;
	}
	public float getCoutsInscription() {
		return coutsInscription;
	}
	public void setCoutsInscription(float coutsInscription) {
		this.coutsInscription = coutsInscription;
	}
	public String getNumeroFacture() {
		return numeroFacture;
	}
	public void setNumeroFacture(String numeroFacture) {
		this.numeroFacture = numeroFacture;
	}
	public Date getDateFacture() {
		return dateFacture;
	}
	public void setDateFacture(Date dateFacture) {
		this.dateFacture = dateFacture;
	}
	public float getMontantFacture() {
		return montantFacture;
	}
	public void setMontantFacture(float montantFacture) {
		this.montantFacture = montantFacture;
	}
	public float getMontantRemuneration() {
		return montantRemuneration;
	}
	public void setMontantRemuneration(float montantRemuneration) {
		this.montantRemuneration = montantRemuneration;
	}
	public Date getPaimentFacture() {
		return paimentFacture;
	}
	public void setPaimentFacture(Date paimentFacture) {
		this.paimentFacture = paimentFacture;
	}
	public Date getAnnulation() {
		return annulation;
	}
	public void setAnnulation(Date annulation) {
		this.annulation = annulation;
	}
	public String getSpécificite() {
		return spécificite;
	}
	public void setSpécificite(String spécificite) {
		this.spécificite = spécificite;
	}
	public int getIdPrestationMere() {
		return idPrestationMere;
	}
	public void setIdPrestationMere(int idPrestationMere) {
		this.idPrestationMere = idPrestationMere;
	}
	public ReccurenceEntity getReccurence() {
		return reccurence;
	}
	public void setReccurence(ReccurenceEntity reccurence) {
		this.reccurence = reccurence;
	}
	public TypePaimentEntity getTypePaiment() {
		return typePaiment;
	}
	public void setTypePaiment(TypePaimentEntity typePaiment) {
		this.typePaiment = typePaiment;
	}
	public MotifAnnulationEntity getMotifAnnulation() {
		return motifAnnulation;
	}
	public void setMotifAnnulation(MotifAnnulationEntity motifAnnulation) {
		this.motifAnnulation = motifAnnulation;
	}
	
	

}
