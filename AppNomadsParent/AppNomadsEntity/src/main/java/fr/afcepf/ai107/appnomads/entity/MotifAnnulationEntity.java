package fr.afcepf.ai107.appnomads.entity;

import java.util.List;

public class MotifAnnulationEntity
{
	int idMotifAnnulation;
	String libelleMotifAnnulation;
	
	List<PrestationEntity> prestations;

	/**
	 * @param idMotifAnnulation
	 * @param libelleMotifAnnulation
	 * @param prestations
	 */
	public MotifAnnulationEntity(int idMotifAnnulation, String libelleMotifAnnulation,
			List<PrestationEntity> prestations)
	{
		super();
		this.idMotifAnnulation = idMotifAnnulation;
		this.libelleMotifAnnulation = libelleMotifAnnulation;
		this.prestations = prestations;
	}

	public int getIdMotifAnnulation() {
		return idMotifAnnulation;
	}

	public void setIdMotifAnnulation(int idMotifAnnulation) {
		this.idMotifAnnulation = idMotifAnnulation;
	}

	public String getLibelleMotifAnnulation() {
		return libelleMotifAnnulation;
	}

	public void setLibelleMotifAnnulation(String libelleMotifAnnulation) {
		this.libelleMotifAnnulation = libelleMotifAnnulation;
	}

	public List<PrestationEntity> getPrestations() {
		return prestations;
	}

	public void setPrestations(List<PrestationEntity> prestations) {
		this.prestations = prestations;
	}
	
	
}
